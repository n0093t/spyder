Сборка образов
-----------
```
docker-compose build
```
Запуск окружения
-----------
```
docker-compose run --rm spyder -h
```
Инициализация базы данных
-----------
```
docker-compose run --rm spyder create-db
docker-compose run --rm spyder migrate
```
Работа со скриптом
-----------
Загрузка ссылок в хранилище
```
docker-compose run --rm spyder --url http://ria.ru --depth 1 load
```
Получение ссылок из хранилища
```
docker-compose run --rm spyder --url http://ria.ru --depth 100 get
```