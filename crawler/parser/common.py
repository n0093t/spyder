from abc import ABC, abstractmethod
from typing import Iterable


class ParseResult:
    def __init__(self, title: str, urls: Iterable[str]):
        """
        Конструктор
        Args:
            title: Заголовок
            urls: Список ссылок
        """
        self.title = title
        self.urls = urls


class RawContent:
    def __init__(self, url: str, text: str):
        """
        Конструктор
        Args:
            url: URL с которого получен контент
            text: Тело страницы
        """
        self.url = url
        self.text = text


class AbstractParser(ABC):
    @classmethod
    @abstractmethod
    def parse(cls, content: RawContent) -> ParseResult:
        raise NotImplementedError
