from io import StringIO
from typing import Set
from urllib import parse

from lxml import etree

from .common import AbstractParser, ParseResult, RawContent
from .exceptions import ParseError


class LXMLParser(AbstractParser):
    @staticmethod
    def _get_title(tree) -> str:
        """
        Получает текст из тэга <title>
        Args:
            tree: ElementTree

        Returns: Заголовок или пустую строку

        """
        title_matches = tree.xpath('/html/head/title/text()')
        title = ''
        if title_matches:
            title = str(title_matches[0])

        return title

    @classmethod
    def _get_urls(cls, base_url: str, tree) -> Set[str]:
        """
        Получает значения атрибута href из тэгов <a>
        Args:
            tree: ElementTree

        Returns: Список ссылок

        """
        url_matches = tree.xpath('//a/@href')
        urls = set()
        for url in url_matches:
            url = cls._normalize_url(base_url, str(url))
            if not url:
                continue

            urls.add(url)

        return urls

    @staticmethod
    def _normalize_url(base_url: str, url: str) -> str or None:
        """
        Нормализует URL: режет якоря, склеивает относительные URL с базовым,
            игнорирует схемы отличные от http и https если они заданы.
        Args:
            base_url: URL, на котором получена ссылка
            url: URL

        Returns: Нормализованный URL

        """
        allowed_schemes = ['http', 'https']
        url_parts = parse.urlparse(url)
        if url_parts.scheme and url_parts.scheme not in allowed_schemes:
            return None

        base_url_parts = parse.urlparse(base_url)

        if url_parts.netloc:
            path = url_parts.path
        else:
            path = parse.urljoin(base_url_parts.path, url_parts.path)

        url_parts_result = [
            url_parts.scheme or base_url_parts.scheme,
            url_parts.netloc or base_url_parts.netloc,
            path or '/',
            url_parts.params,
            url_parts.query,
            ''
        ]

        return parse.urlunparse(url_parts_result)

    @classmethod
    def parse(cls, content: RawContent) -> ParseResult:
        """
        Извлекает текст из тэга <title> и значение атрибута
        href для всех тэгов <a>
        Args:
            content: Контент

        Returns: Заголовок и ссылки

        """
        parser = etree.HTMLParser()

        try:
            tree = etree.parse(StringIO(content.text), parser)
            title = cls._get_title(tree)
            urls = cls._get_urls(content.url, tree)
        except Exception:
            raise ParseError

        return ParseResult(
            title=title,
            urls=urls
        )
