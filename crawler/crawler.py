import asyncio
from asyncio.queues import Queue
from concurrent.futures import ProcessPoolExecutor

import uvloop
from aiohttp import ClientSession, ClientTimeout

from storage.common import AbstractStorage
from .parser import lxml_parser
from .parser.common import AbstractParser, RawContent
from .parser.exceptions import ParseError


class SiteData:
    def __init__(self, site_url: str, url: str, title: str, html: str or None = None):
        self.site_url = site_url
        self.url = url
        self.title = title
        self.html = html


class Site:
    def __init__(self, url: str, depth: int, content: str or None = None):
        """
        Конструктор
        Args:
            url: Базовый URL
            depth: Глубина обхода
            content: Тело страницы
        """

        self.depth = depth
        self.url = url
        self.content = content


class Crawler:

    _parser = lxml_parser.LXMLParser

    def __init__(self, url: str, depth: int, storage: AbstractStorage, max_workers: int = 10,
                 parser: AbstractParser = None):
        """
        Конструктор
        Args:
            url: Базовый URL
            depth: Глубина обхода
            storage: Хранилище, в котором будем хранить данные
            max_workers: Максимальное количество тредов-потребителей
            parser: Парсер
        """
        uvloop.install()

        self._url = url
        self._writer_queue: Queue = None
        self._depth = depth
        self._parsed_urls = set()
        self._max_workers = max_workers
        self._storage = storage
        self._parse_executor = ProcessPoolExecutor()
        self._queue: Queue = None

        if parser is not None:
            self._parser = parser

    @staticmethod
    async def _send_request(url: str) -> str or None:
        """
        Загружает страницу по URL
        Args:
            url: URL страницы

        Returns: Тело страницы или None

        """
        timeout = ClientTimeout(total=5)

        try:
            async with ClientSession(timeout=timeout) as session:
                async with session.get(url) as response:
                    content = await response.text()
                    return content.strip()
        except Exception:
            return None

    async def _worker(self):
        """Получает данные из очереди, загружает и отправляет на
        парсинг
        """
        loop = asyncio.get_running_loop()
        while True:
            try:
                site: Site = await self._queue.get()
            except asyncio.CancelledError:
                break

            content = await self._send_request(site.url)

            if not content:
                self._queue.task_done()
                continue

            parse_result = await loop.run_in_executor(
                self._parse_executor,
                self._parse_worker,
                RawContent(url=site.url, text=content)
            )

            self._parsed_urls.add(site.url)

            if parse_result:
                urls = parse_result.urls - self._parsed_urls
                if site.depth > 0:
                    for url in urls:
                        await self._queue.put(Site(url=url, depth=site.depth - 1))

                await self._writer_queue.put(
                    SiteData(
                        site_url=self._url,
                        url=site.url,
                        title=parse_result.title,
                        html=content
                    )
                )

            self._queue.task_done()

    @classmethod
    def _parse_worker(cls, content: RawContent):
        """
        Парсит контент
        Args:
            content: Данные

        Returns: Заголовок и ссылки со страницы

        """
        try:
            return cls._parser.parse(content=content)
        except ParseError:
            return None

    async def _write_worker(self):
        """Пишет данные страницы в базу данных"""
        while True:
            try:
                site_data: SiteData = await self._writer_queue.get()
            except asyncio.CancelledError:
                break

            await self._storage.create_url(
                site_url=self._url,
                url=site_data.url,
                title=site_data.title,
                html=site_data.html
            )
            self._writer_queue.task_done()

    def _init_queues(self):
        """Инициализирует очереди"""
        self._queue = Queue()
        self._writer_queue = Queue()

    async def _delete_site_data(self, site_url: str):
        """Удаляет данные сайта по URL"""
        await self._storage.delete_urls_by_site_url(site_url=site_url)

    async def start(self):
        """Запускает воркеров"""
        await self._delete_site_data(self._url)
        self._init_queues()
        await self._queue.put(Site(url=self._url, depth=self._depth))

        workers = []
        for _ in range(self._max_workers):
            workers.append(self._worker())

        workers.append(self._write_worker())

        tasks = []
        for worker in workers:
            tasks.append(asyncio.create_task(worker))

        await self._queue.join()
        await self._writer_queue.join()

        for task in tasks:
            task.cancel()

        await asyncio.gather(*tasks)
