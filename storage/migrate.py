import psycopg2
from peewee_migrate import Router
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

from .conf import DATABASE


def migrate():
    from .db import db

    router = Router(db)

    router.create(auto='storage.models')
    router.run()


def create_database():
    db_init_query = '''
    CREATE DATABASE spyder WITH encoding=utf8;
    '''

    conn = psycopg2.connect(host=DATABASE['host'], user=DATABASE['user'])
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cur = conn.cursor()

    cur.execute(db_init_query)

    cur.close()
    conn.close()


if __name__ == '__main__':
    migrate()
