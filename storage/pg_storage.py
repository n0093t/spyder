from typing import Iterable

from .common import AbstractStorage
from .db import manager
from .models import Url


class PostgresStorage(AbstractStorage):
    @classmethod
    async def create_url(cls, site_url: str, url: str, title: str, html: str):
        """
        Создает запись с информацией об URL
        Args:
            site_url: Базовый URL
            url: URL страницы
            title: Заголовок страницы
            html: Тело страницы

        """
        await manager.create(Url, site_url=site_url, url=url, title=title, html=html)

    @classmethod
    async def get_urls_by_site_url(cls, site_url: str, limit: int) -> Iterable[Url]:
        """
        Получает ссылки по URL базового сайта
        Args:
            site_url: Базовый URL
            limit: Ограничение выборки

        Returns: Список ссылок

        """
        query = Url.select().where(Url.site_url == site_url)
        if limit:
            query = query.limit(limit)

        return await manager.execute(query)

    @classmethod
    async def delete_urls_by_site_url(cls, site_url: str):
        """
        Удаляет ссылки по базовому URL
        Args:
            site_url: Базовый URL

        """
        query = Url.delete().where(Url.site_url == site_url)

        await manager.execute(query)
