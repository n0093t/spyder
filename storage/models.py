from peewee import Model, CharField, TextField

from .db import db


class Url(Model):
    """Модель хранящая данные ссылок."""
    site_url = CharField(max_length=1024, index=True)
    url = CharField(max_length=1024)
    title = CharField(max_length=1024)
    html = TextField()

    class Meta:
        database = db
