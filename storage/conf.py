from envparse import env

DATABASE = {
    'host': env('POSTGRESQL_HOST'),
    'port': env('POSTGRESQL_PORT', default=5432, cast=int),
    'dbname': env('POSTGRESQL_DATABASE'),
    'user': env('POSTGRESQL_USER', default='postgres'),
    'password': env('POSTGRESQL_PASSWORD', default=None)
}
