from peewee_async import Manager, PooledPostgresqlDatabase

from .conf import DATABASE

db = PooledPostgresqlDatabase(
    DATABASE['dbname'],
    host=DATABASE['host'],
    port=DATABASE['port'],
    user=DATABASE['user'],
    password=DATABASE['password'],
)

manager = Manager(db)
