from abc import ABC, abstractmethod
from typing import Iterable

from .models import Url


class AbstractStorage(ABC):
    @classmethod
    @abstractmethod
    async def create_url(cls, site_url: str, url: str, title: str, html: str):
        raise NotImplementedError

    @classmethod
    @abstractmethod
    async def get_urls_by_site_url(cls, site_url: str, limit: int) -> Iterable[Url]:
        raise NotImplementedError

    @classmethod
    @abstractmethod
    async def delete_urls_by_site_url(cls, site_url: str):
        raise NotImplementedError
