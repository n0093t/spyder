FROM python:3.7

COPY . /spyder
WORKDIR /spyder
RUN pip install -Ur requipments.txt

ENTRYPOINT ["python", "entrypoint.py"]
