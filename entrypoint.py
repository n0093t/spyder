import argparse
import asyncio
import time

import uvloop
from memory_profiler import memory_usage

from crawler.crawler import Crawler
from storage import storage, migrate

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='spy.py', add_help=True)

    group_db = parser.add_argument_group('Operations')
    group_db.add_argument('operation', choices=['create-db', 'migrate', 'load', 'get'])

    group_spy = parser.add_argument_group('Spyder operations (load or get)')
    group_spy.add_argument('--url', type=str, help='Site url')
    group_spy.add_argument('--depth', type=int, help='Depth')
    group_spy.add_argument('--max-workers', type=int, default=100, help='Max workers')

    args = parser.parse_args()

    uvloop.install()

    if args.operation == 'load':
        crawler = Crawler(url=args.url, depth=args.depth, storage=storage, max_workers=args.max_workers)
        started_at = time.monotonic()
        max_memory_usage = memory_usage(
            (asyncio.run, [crawler.start()], {}), max_usage=True
        )[0]
        execution_time = time.monotonic() - started_at
        print('ok, execution time: {:.2f}s, peak memory usage: {:.2f} Mb'.format(execution_time, max_memory_usage))
    elif args.operation == 'get':
        urls = asyncio.run(storage.get_urls_by_site_url(site_url=args.url, limit=args.depth))

        for url in urls:
            print(url.url, ':', url.title)
    elif args.operation == 'create-db':
        migrate.create_database()
        print('Database created')
    elif args.operation == 'migrate':
        migrate.migrate()
    else:
        parser.print_help()
